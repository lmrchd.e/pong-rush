/**
 * Fonctions nécessaires et connues par les fonctions setup() draw() et keyPressed() :
 *
 * Nom                                                                                                 | Description
 * ====================================================================================================================================================
 * ellipse(position_horizontale, position_verticale, taille_point)"                                    | Dessine un point
 * ----------------------------------------------------------------------------------------------------------------------------------------------------
 * rect(position_horizontale_coin_haut_gauche, position_verticale_coin_haut_gauche, largeur, hauteur)" | Dessine un rectangle
 * ----------------------------------------------------------------------------------------------------------------------------------------------------
 * text(texte, position_horizontale, position_verticale)"                                              | Ecrit du texte
 * ----------------------------------------------------------------------------------------------------------------------------------------------------
 * random([premiere_valeur, deuxieme_valeur, ...])                                                     | Retoune une valeur au hasard parmis plusieurs
 * ----------------------------------------------------------------------------------------------------------------------------------------------------
 *
 * Pour les connaitre les autres fonctions, consulter "https://p5js.org/reference".
 */



/**
 * Fonction appelée automatiquement une fois au chargement de la page.
 * Les variables qui y sont initialisées sont connu par les fonctions draw() et keyPressed().
 */
function setup() {
    createCanvas(600, 400); // Ne pas enlever (sinon, c'est tout petit :))

    width; // Cette variable contient la largeur du canvas.
    height; // Cette variable contient la hauteur du cavans.

    positions = [1, 2, -1, -2];

    playerA = {
        x: 1,
        y: (height / 2) -50,
        score: 0
    };

    playerB = {
        x: width - 11,
        y: (height / 2) -50,
        score: 0
    };

    ball = {
        x: width / 2,
        y: height / 2,
        size: 10,
        speed: 1
    };

    movement = {
        frequency: 0,
        current: 0,
        x: random(positions),
        y: random(positions)
    };

    // Ton code ici
}



/**
 * Fonction appelée automatiquement à intervalles de temps réguliers (tous les x millisecondes).
 */
function draw() {
    background(0); // Ne pas enlever (sinon, c'est tout blanc :))
    fill(255);  // Ne pas enlever (pour afficher le texte en blanc)

    width; // Cette variable contient la largeur du canvas.
    height; // Cette variable contient la hauteur du cavans.

    text(playerA.score, 30, 30);
    text(playerB.score, width-30, 30);
    ellipse(ball.x, ball.y, ball.size);
    rect(playerA.x, playerA.y, 10, 100);
    rect(playerB.x, playerB.y, 10, 100);

    movement.current += 1;
    if (movement.current >= movement.frequency) {
        movement.current = 0;
        ball.x += movement.x;
        ball.y += movement.y;
    }

    if (ball.y >= height || ball.y <= 0) {
        movement.y = -movement.y;
    }

    if (ball.x >= width) {
        playerA.score += 1;
        ball.x = width / 2;
        ball.y = height / 2;
        movement.y = random(positions);
        movement.x = random(positions);
    }

    if (ball.x <= 0) {
        playerB.score += 1;
        ball.x = width / 2;
        ball.y = height / 2;
        movement.y = random(positions);
        movement.x = random(positions);
    }

}

/**
 * Fonction appelée automatiquement quand on appuie sur une touche du clavier.
 */
function keyPressed() {
    key; // Cette variable contient la lettre tapée au clavier EN MAJUSCULE.

    switch (key) {
        case "A"://move up left
            playerA.y -= 10;
            break;
        case "Q"://move down left
            playerA.y += 10;
            break;
        case "P"://move up right
            playerB.y -= 10;
            break;
        case "M"://move down right
            playerB.y += 10;
            break;
    }
    return key;
}

